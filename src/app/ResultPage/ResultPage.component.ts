import { Component } from '@angular/core';
// import { PlatformLocation } from '@angular/common';

@Component({
    selector: 'Resultpage',
    templateUrl: './ResultPage.component.html',
    styleUrls: ['./ResultPage.component.css']
})


export class AppResult {

    final: any;
    percent: number;
    correct_answers: number;
    total_qus: number;
    constructor() {
        this.final = JSON.parse(localStorage.getItem('final_result'));
        // alert(); 
        console.log(this.final);
        // this.percent = ((this.final.correct_answers) / (this.final.total_qus)) * 100;
    }

    getStyle(crct, user_option, opt) {
        var color = {};
        if (crct == opt) {
            color['color'] = 'green';
        }
        else if (user_option == opt && opt != crct) {
            color['color'] = 'red';
        }

        return color;
    }
}