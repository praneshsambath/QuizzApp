import { TestBed, async, fakeAsync, tick } from '@angular/core/testing';
import { HttpModule } from '@angular/http';
import { AppSignUp } from 'app/SignUp/SignUp.component';
import { RouterTestingModule } from '@angular/router/testing';
import { FormsModule } from '@angular/forms';
import { Router } from '@angular/router/src/router';
import { routes } from 'app/app.module';
import { AppHome } from 'app/HomePage/HomePage.component';
import { Location } from '@angular/common';
import { AppContactUs } from 'app/ContactUs/ContactUs.component';
import { AppQuestion } from 'app/QuestionPage/QuestionPage.component';
import { AppResult } from 'app/ResultPage/ResultPage.component';
import { AppComponent } from 'app/app.component';
import { NgForm } from '@angular/forms/src/directives/ng_form';
import { Component } from '@angular/core/src/metadata/directives';
import { Http } from '@angular/http/src/http';



describe('AppSignUp', function () {
    var app: any;
    var app1: any;
    beforeEach(() => {



        TestBed.configureTestingModule({

            imports: [RouterTestingModule.withRoutes(routes), FormsModule],
            declarations: [AppComponent, AppHome, AppSignUp, AppContactUs, AppQuestion, AppResult],
            providers: [Http]

        });
        TestBed.compileComponents();
        const fixture = TestBed.createComponent(AppSignUp);
        app = fixture.componentInstance;
        const fixture1 = TestBed.createComponent(AppComponent);
        app1 = fixture1.componentInstance;
    });
    it('should create the app', function () {


        expect(app).toBeTruthy();
    });
    it('name field should clear', function () {

        app.name = 'pranesh';
        app.clearall(app.name, app.mobile, app.email);

        expect(app.name).toEqual('');
    });

    it('Email field should get clear', function () {


        app.email = 'praneshsambath@gmail.com';
        app.clearall(app.name, app.mobile, app.email);

        expect(app.email).toEqual('');
    });

    it(' mobile field should get clear', function () {

        app.mobile = '9843279919';
        app.clearall(app.name, app.mobile, app.email);

        expect(app.mobile).toEqual('');
    });

})
