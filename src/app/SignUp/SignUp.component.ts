import { Component } from '@angular/core';
import { Router } from '@angular/router';
// import { NgForm } from '@angular/forms/src/directives/ng_form';
// import { FormsModule }   from '@angular/forms';


@Component({
    selector: 'sign-up',
    templateUrl: '../SignUp/SignUp.component.html',
    styleUrls: ['../SignUp/SignUp.component.css']
})

export class AppSignUp {

    name: string = '';
    email: string = '';
    mobile: string = '';

    clearall(name, email, mobile) {

        this.name = '';
        this.email = '';
        this.mobile = '';
    }
    press(value) {
        var pattern = /[0-9\+\-\ ]/;
        console.log(value);//TO understand console this full function and see
        // console.log(value.charCode);//Stores ASCII values
        let mobile = String.fromCharCode(value.charCode);
        if (value.keycode != 8 && !pattern.test(mobile)) {
            event.preventDefault();
        }
    }
    constructor(private router: Router) { }
    onSubmit(values: any) {
        alert(values);
        console.log(values);
        this.router.navigate(['/HomePage']);
    }
}
