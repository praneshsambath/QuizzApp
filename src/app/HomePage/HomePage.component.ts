import { Component, OnInit } from '@angular/core';
import { HomeService } from '../Service/myservice.service';
import { Http } from '@angular/http';


import { Router } from '@angular/router';

@Component({
    selector: 'home-page',
    templateUrl: '../HomePage/HomePage.component.html',
    styleUrls: ['../HomePage/HomePage.component.css'],
    providers: [HomeService]
})

export class AppHome implements OnInit {
    subjects = [];
    units = [];
    easy: any;
    normal: any;
    hard: any;
    mcq: any[] = [];


    constructor(private homeService: HomeService, private http: Http, private router: Router) { }

    ngOnInit() {
        this.getSubjects();
    }

    getSubjects() {

        this.homeService.getSubjects().subscribe(

            data => {
                this.subjects = data;
                // console.log(this.subjects);
            },
            err => console.log(err)
        )

    }

    onChange(subject_id) {

        this.homeService.getUnit(subject_id).subscribe(
            data => this.units = data,
            err => console.log(err)
        )
    }
    complexity(): boolean {
        var level = (this.easy) + (this.normal) + (this.hard);
        if (level == 100) {
            return true;
        }
        else {
            return false;
        }
    }
    navigateTo() {
        this.router.navigate(['/QuestionPage']);

    }
    onSubmit(value) {
        this.homeService.questions(value).subscribe(
            data => {
                this.mcq = data;
                localStorage.setItem('tokenKey', JSON.stringify(this.mcq));
                this.navigateTo();
            },
            err => console.log(err)
        )
    }
}

