import { TestBed, inject, async } from '@angular/core/testing';
import { HttpModule } from '@angular/http';
import { AppHome } from './HomePage.component';
import { AppSignUp } from 'app/SignUp/SignUp.component';
import { AppContactUs } from 'app/ContactUs/ContactUs.component';
import { RouterTestingModule } from '@angular/router/testing';
import { FormsModule } from '@angular/forms';
import { HomeService } from 'app/Service/myservice.service';
import { MockBackend, MockConnection } from '@angular/http/testing';
import { Connection } from '@angular/http/src/interfaces';
import { ResponseOptions } from '@angular/http/src/base_response_options';
import { AppQuestion } from 'app/QuestionPage/QuestionPage.component';
import { AppComponent } from 'app/app.component';
import { AppResult } from 'app/ResultPage/ResultPage.component';
import { routes } from 'app/app.module';
import { BrowserModule } from '@angular/platform-browser/src/browser';
import { Router } from '@angular/router/src/router';
import { Http, BaseRequestOptions, Response } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/observable/of';
import { NgForm } from '@angular/forms/src/directives/ng_form';




describe('HomePage', () => {

  let router = { navigate: jasmine.createSpy('navigate') }


  beforeEach(() => {
    TestBed.configureTestingModule({

      declarations: [AppHome, AppSignUp, AppContactUs, AppQuestion, AppComponent, AppResult],
      imports: [FormsModule, HttpModule, RouterTestingModule.withRoutes(routes), BrowserModule],
      providers: [HomeService, {
        provide: Router,
        useValue: router
      }]
    })
    TestBed.compileComponents();
  });

  it('should create the app', function () {
    const fixture = TestBed.createComponent(AppHome);
    const app = fixture.componentInstance;
    expect(app).toBeTruthy();
  })
  it('Should be valid only if Easy+Normal+Hard=100', () => {

    const fixture = TestBed.createComponent(AppHome);
    const app = fixture.componentInstance;
    app.easy = 30;
    app.hard = 50;
    app.normal = 20;
    const result = app.complexity();
    console.log(result);
    expect(result).toBeTruthy();
  })
  it('should not be valid if easy+normal+hard is not equal to 100', () => {

    const fixture = TestBed.createComponent(AppHome);
    const app = fixture.componentInstance;
    app.easy = 50;
    app.normal = 50;
    app.hard = 30;
    const result = app.complexity();
    console.log(result);
    expect(result).toBeFalsy();
  })
  it('Should navigate to Question Page', function () {
    const fixture = TestBed.createComponent(AppHome);
    const app = fixture.componentInstance;

    app.navigateTo();

    expect(router.navigate).toHaveBeenCalledWith(['/QuestionPage']);

  })
})

//FOR USING SERVICE BY MOCKBACKEND


beforeEach(() => {
  TestBed.configureTestingModule({

    imports: [FormsModule, HttpModule, RouterTestingModule.withRoutes(routes), BrowserModule],
    declarations: [AppComponent, AppSignUp, AppContactUs, AppHome, AppQuestion, AppResult],
    providers: [
      { provide: HomeService, useClass: MockHomeService },
      MockHomeService,
      MockBackend
    ]
  })
});
var correct_answers,
  total_qus,
  wrong_answer,
  sub, id, unit,
  quest, value,
  subject_id, unit_id,
  unit_name, question,
  option_a, option_b,
  option_c;


class MockHomeService {

  //MOCKING  BACKEND API OF getSubjects() in service
  getSubjects(): Observable<any> {
    return Observable.of({
      'correct_answers': '4', 'total_qus': '10', 'wrong_answer': '6', 'userquestions':
        {
          'crct_ans'
            :
            "23",
          'option_a'
            :
            "23",
          'option_b'
            :
            "61",
          'option_c'
            :
            "3",
          'qustion'
            :
            "19 + ……. = 42",
          'user_option'
            :
            "61"
        }
    })
  }
  //MOCKING  BACKEND API OF getUnit() in service
  getUnit(id): Observable<any> {
    return Observable.of({ 'subject_id': '1', 'unit_id': '2', 'unit_name': 'java' })
  }
  //MOCKING  BACKEND API OF questions() in service
  questions(value): Observable<any> {
    return Observable.of({ 'question': '1+2+3+4=', 'option_a': '10', 'option_b': '12', 'option_c': '25' })
  }
}





//Test Case for getsubjects()
it('should call the api in getSubjects() ',
  (inject([HomeService, MockBackend], (homeservice: HomeService, mockbackend: MockBackend) => {
    homeservice.getSubjects().subscribe(
      data => {
        sub = data;
        console.log(sub)
      }
    );
    expect(sub).toBeDefined(correct_answers);
    expect(sub).toBeDefined(total_qus);
    expect(sub).toBeDefined(wrong_answer);
  })));





//Test Case for getUnit()
it('Should call api in getUnit()',
  (inject([HomeService, MockBackend], (homeservice: HomeService, mockbackend: MockBackend) => {
    homeservice.getUnit(id).subscribe(
      data => { unit = data; console.log(unit) }
    );
    expect(unit).toBeDefined(subject_id)
    expect(unit).toBeDefined(unit_id)
    expect(unit).toBeDefined(unit_name)
  })))






//TestCase for Question()
it('Should call api in Question()',
  (inject([HomeService, MockBackend], (homeservice: HomeService, mockbackend: MockBackend) => {

    homeservice.questions(value).subscribe(
      data => { quest = data; console.log(quest) }
    );
    expect(quest).toBeDefined(question)
    expect(quest).toBeDefined(option_b)
    expect(quest).toBeDefined(option_c)
  })))





it('Should render h1 tag', function () {

  const fixture = TestBed.createComponent(AppHome);
  fixture.detectChanges();
  const compiled = fixture.debugElement.nativeElement;
  expect(compiled.querySelector('h1').textContent).toContain('Home Page');
})
