import { Injectable } from '@angular/core';
import { Http, Headers, RequestOptions } from '@angular/http';

import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';

@Injectable()
export class HomeService {

    private headers = new Headers({ 'Content-Type': 'application/json', 'charset': 'UTF-8' });
    private options = new RequestOptions({ headers: this.headers });


    constructor(private http: Http) { }

    getSubjects(): Observable<any> {
        // console.log('service')
        return this.http.get('http://192.168.1.70:8080/QuizApplication/rest/subjects').map(res => res.json());
    }

    getUnit(id): Observable<any> {

        return this.http.get('http://192.168.1.70:8080/QuizApplication/rest/subject/' + id).map(res => res.json());

    }
    questions(value): Observable<any> {
        // console.log(value);
        //  console.log('http://192.168.1.95:8080/QuizApplication/rest/retrievequstions?subject=' + value.subject + '&unit=' + value.unit + '&totalqus=' + value.qst + '&simple=' + value.easy + '&medium=' + value.normal + '&hard=' + value.hard + '');
        return this.http.get('http://192.168.1.70:8080/QuizApplication/rest/retrievequstions?subject=' + value.subject + '&unit=' + value.unit + '&totalqus=' + value.qst + '&simple=' + value.easy + '&medium=' + value.normal + '&hard=' + value.hard + '').map(res => res.json());
    }
    Final_Result(selAns): Observable<any> {

        // let result=JSON.stringify(selAns);
        // console.log(selAns);
        return this.http.post('http://192.168.1.70:8080/QuizApplication/rest/retrieveanswers', selAns).map(res => res.json());
    }

}

