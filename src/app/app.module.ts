import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { RouterModule, Routes } from '@angular/router';

import { AppComponent } from './app.component';
import { AppSignUp } from './SignUp/SignUp.component';
import { AppContactUs } from './ContactUs/ContactUs.component';
import { AppHome } from './HomePage/HomePage.component';
import { AppQuestion } from './QuestionPage/QuestionPage.component';
// import { Ng2PaginationModule } from 'ng2-pagination';
import { AppResult } from './ResultPage/ResultPage.component';


export const routes: Routes = [

  { path: "SignUp", component: AppSignUp },
  { path: "ContactUs", component: AppContactUs },
  { path: "HomePage", component: AppHome },
  { path: "QuestionPage", component: AppQuestion },
  { path: "ResultPage", component: AppResult }
]


@NgModule({
  declarations: [AppComponent, AppSignUp, AppContactUs, AppHome, AppQuestion, AppResult],
  imports: [
    BrowserModule,
    FormsModule,
    ReactiveFormsModule,
    HttpModule,
    // Ng2PaginationModule,
    RouterModule.forRoot(routes)
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
