import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { HomeService } from '../Service/myservice.service';
import { Http } from '@angular/http';

@Component({
    selector: 'Questionpage',
    templateUrl: './QuestionPage.component.html',
    styleUrls: ['./QuestionPage.component.css'],
    providers: [HomeService]
})

export class AppQuestion {
    public qstn: any[];
    selAns: any[] = [];
    result: any;

    constructor(private resultService: HomeService, private http: Http, private router: Router) {
        this.qstn = JSON.parse(localStorage.getItem('tokenKey'));
        // console.log(this.qstn);
    }

    navigateTo() {
        // alert();
        console.log("paka");
        this.router.navigate(['/ResultPage']);
    }
    onSubmit() {

        for (var i = 0; i < this.qstn.length; i++) {
            this.selAns.push({ id: this.qstn[i].id, answers: this.qstn[i].answers });

        }

        this.resultService.Final_Result(this.selAns).subscribe(
            data => {
                this.result = JSON.stringify(data);
                localStorage.setItem('final_result', this.result);
                // console.log(this.result);
                this.navigateTo();
            },
            err => console.log(err)
        )
    }



}   