import { AppQuestion } from 'app/QuestionPage/QuestionPage.component';
import { TestBed, inject, async } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { routes, AppModule } from 'app/app.module';
import { FormsModule } from '@angular/forms';
import { AppComponent } from 'app/app.component';
import { AppContactUs } from 'app/ContactUs/ContactUs.component';
import { AppHome } from 'app/HomePage/HomePage.component';
import { AppSignUp } from 'app/SignUp/SignUp.component';
import { AppResult } from 'app/ResultPage/ResultPage.component';
import { Http, BaseRequestOptions, Response } from '@angular/http/';
import { NgForm } from '@angular/forms/src/directives/ng_form';
import { HttpModule } from '@angular/http/src/http_module';
import { BrowserModule } from '@angular/platform-browser/src/browser';
import { HomeService } from 'app/Service/myservice.service';
import { Router } from '@angular/router/src/router';
import { MockBackend, MockConnection } from '@angular/http/testing';
import { Connection } from '@angular/http/src/interfaces';
import { Observable } from 'rxjs/Observable';
import { ResponseOptions } from '@angular/http/src/base_response_options';
import 'rxjs/add/observable/of';





describe("Questionpage", function () {





    let router = { navigate: jasmine.createSpy('navigate') }
    var app, selAns, endResult, sel_ans, correct_ans, wrong_ans;






    class CreateHomeService {

        Final_Result(selAns): Observable<any> {
            return Observable.of({ 'sel_Ans': '6', 'correct_ans': '3', 'wrong_ans': '3' })
        }
    }






    beforeEach(() => {
        TestBed.configureTestingModule({


            imports: [RouterTestingModule.withRoutes(routes), HttpModule, BrowserModule, FormsModule],
            declarations: [AppComponent, AppSignUp, AppContactUs, AppHome, AppQuestion, AppResult],
            providers: [HomeService,
                {
                    provide: HomeService, UseClass: CreateHomeService
                },
                MockBackend,
                CreateHomeService,
                {
                    provide: Router,
                    useValue: router
                }]
        })
        TestBed.compileComponents();
        const fixtures = TestBed.createComponent(AppQuestion);
        app = fixtures.componentInstance;

    })




    it('should render title in a h1 tag', async(() => {
        const fixture = TestBed.createComponent(AppQuestion);
        fixture.detectChanges();
        const compiled = fixture.debugElement.nativeElement;
        expect(compiled.querySelector('h1').textContent).toContain('Question Page');
    }));






    it('Should create APP', function () {
        expect(app).toBeTruthy();
    })









    it('Should navigate to Result page', function () {
        app.navigateTo();
        expect(router.navigate).toHaveBeenCalledWith(['/ResultPage']);
    })








    // beforeEach(() => {
    //     TestBed.configureTestingModule({

    //         imports: [FormsModule, HttpModule, RouterTestingModule.withRoutes(routes), BrowserModule],
    //         declarations: [AppComponent, AppSignUp, AppContactUs, AppHome, AppQuestion, AppResult],
    //         providers: [
    //             {
    //                 provide: HomeService, UseClass: CreateHomeService
    //             },
    //             MockBackend,
    //             CreateHomeService

    //         ]

    //     })

    // })

    it('should call the api in Final_Result() ',
        (inject([HomeService, MockBackend], (homeservice: HomeService, mockbackend: MockBackend) => {

            homeservice.Final_Result(selAns).subscribe(
                data => { endResult = data; console.log(endResult) }
            );
        }))
    );

    // expect(final_result).not.toBe(null)
})