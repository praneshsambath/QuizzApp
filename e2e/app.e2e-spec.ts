import { QuizzzPage } from './app.po';

describe('quizzz App', function() {
  let page: QuizzzPage;

  beforeEach(() => {
    page = new QuizzzPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
